# Tutorial: OIDC and Multi-Account Deployment with GitLab CI/CD and ECS

## Overview
This main objective of this tutorial is to demonstrate how to use [OpenID Connect (OIDC) and GitLab](https://docs.gitlab.com/ee/ci/cloud_services/) to do a multi-account deployment to ECS. Through the tutorial, you will provison an environment including OIDC, ECR, and ECS. Once the environment is configured, we will review the GitLab CI/CD constructs that allow a multi-account deployment to ECS with OIDC.

While this tutorial employs ECS and ECR, the OIDC/JWT GitLab features can be setup for any set of AWS services you wish to work with. The target IAM role must simply have access to the services then the IaC code used with the roles can update the services being used. This includes building complete new environments (higher permission level) or deploying into a preexisting infrastructure (lower permission level).

## Prerequisities
  * Two separate AWS accounts to simulate a development and production environment
  * GitLab Account
  * Clone of this repository 
  * Knowledge of Terraform, AWS, AWS ECS, AWS ECR, and basic GitLab CI Constructs


## Architecture Overview

<p align="center">
  <img src="images/diagram.png" alt="isolated" width="600"/>
</p>

## What is OpenID Connect (OIDC)?
In basic terms, it's an authentication protocol without managing credentials or passwords. Rather than storing AWS credentials throughout all your projects as secrets or variables risking exposure, GitLab can accept a AWS role to retrieve temporary credentials using trust-based communication. AWS has a [list of supported services](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_aws-services-that-work-with-iam.html) that allow temporary credentials.


## Repository Overview
### App Folder
This is a primitive Node+Pug application that outputs a simple login UI with a GitLab logo. We will dockerize this application and push this ECR along with aligning the docker image to the ECS task definition.


### Terraform Folder
<ins>Environments Folder</ins>

This folder contains an environment folder for what we will call "Development" And "Production" that point to each AWS account. In each folder contains the following:
* backend.conf: Location pointer to S3 bucket to store the terraform state file.
* main.tf: Provision modules including oidc, ecr, and ecs.
* outputs.tf: Outputs including the GitLab CI OIDC ARN Profile, ECS endpoint via ALB, and ECR URL.
* react_app_task_definition.json: ECS task definition that defines our node application, image, and port.
* terraform.tfvars: Variable definitions for OIDC, ECR, and ECS .
* variables.tf: Terraform variables for AWS, OIDC, ECR, and ECS.


<ins>Modules Folder</ins>

ECR:
* Provisions a private ECR repository

OIDC:
* Creates an OpenID Provider in AWS. This creates trust between AWS and the GitLab instance (by default GitLab.com)
* Creates an IAM role with ECS and ECR access.
* Attaches a AssumeRoleWithWebIdentity policy to the role. This role is used in GitLab CI for OIDC connection. **This policy specifies the GitLab project and what resources it can access in AWS**

ECS:
* Reference to this [terraform repository](https://gitlab.com/jrandazzo/terraform-ecs-gitlab) to provision a non-production ECS environment that can be accessed publicily.


## How-to Tutorial

### Set up the GitLab Project

1. Make a clone of this repository and [create a separate GitLab Project](https://docs.gitlab.com/ee/user/project/#create-a-blank-project)
1. Make note of the group/project path as we will use this for defining terraform variables in this [step](#set-up-terraform-variables-for-each-environment).


### Provision the AWS environment locally
The portion of the tutorial will provision the AWS environments (ODIC,ECR,ECS) locally using terraform and [awscli](https://formulae.brew.sh/formula/awscli).

### Set up AWS credentials for each AWS Account to be used locally
1. In this tutorial, we will follow the [AWS Named Profiles](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html) approach for accessing AWS accounts. Please see these [instructions](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html) on how to create a AWS Named Profile account.
1. Create credentials for each account and assign a Named Profile to each.
1.  Example of AWS config (~/.aws/credentials). Please note the profile names `prod` and `dev`. These profiles will be used going forward.
  
    ```
    [prod]
    aws_access_key_id=<access_key>
    aws_secret_access_key=<secret_key>
    region=us-east-1

    [dev]
    aws_access_key_id=<access_key>
    aws_secret_access_key=<secret_key>
    region=us-east-1
    ```
* Pro-tip: To view resources in AWS UI quickly, use the ["switch-role"](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_switch-role-console.html) function without logging into each AWS account.


### Create an S3 bucket in each account to store the terraform state file
1. An s3 bucket is provisioned in each account with locking enabled to store the terraform state file. This was done outside of terraform using awscli. See the backend.conf file for the naming. You can optionally create the bucket through the UI.

    ```bash
    AWS_PROFILE=<env> aws s3api create-bucket \
    --bucket <env>-oidc-ecs-s3-bucket-tf \
    --object-lock-enabled-for-bucket
    ```
2. Set the backend.conf file to the name of the bucket for the dev account. Repeat the script above for the production account

### Set up terraform variables for each environment
1. Please rename `terraform.tfvars.example` to `terraform.tfvars` within each `dev` and `prod` folder.
1. Modify `match_value`: This is going to align with the GitLab project and branch name. Pay special attention to the "group/project" path and the branch name "main". For this tutorial, we will use a development and main branch name. The "dev" environment folder will point to the development branch whereas "prod" environment folder will point to the main branch. This mapping illustrates environment to branching strategy. In the example below, the group/project path for GitLab is `guided-explorations/aws/oidc-and-multi-account-deployment-with-ecs` and we point to the `main` branch.

    ##### Example of variables file
    ```
    aws_region = "us-east-1"
    prefix     = "prod"

    #oidc module
    aud_value   = "https://ecs.customerapp.com"
    match_field = "sub"
    match_value = ["project_path:guided-explorations/aws/oidc-and-multi-account-deployment-with-ecs:ref_type:branch:ref:main"]

    #ecr module
    ecr_repo_name = "prod-ecr-node-app-repo"

    #ecs module
    ecs_task_definition_file_location = "react_app_task_definition.json"
    ecs_container_name                = "node-app"
    asg_desired                       = 2
    asg_max                           = 2
    desired_tasks                     = 1
    tag_resource                      = "prod-tf-ecs-oidc-multi-account-demo"
    ```
  

### Provision the terraform environment

1. This tutorial follows using the Named profiles for AWS credentials. Feel free to refer to the AWS docs for other credential patterns. `AWS_PROFILE` will be specified prior to running the terraform commands.
2. Provision the "dev" environment
    ```bash
    cd terraform/environments/dev
    export AWS_PROFILE=dev 
    terraform init -backend-config=backend.conf
    terraform plan -var-file="terraform.tfvars"
    terraform apply -var-file="terraform.tfvars"
    ```
3. When `terraform apply` is complete, there are three important output variables to capture that will be used in future steps. The output will look similar to the following:
   ```
    CI_ROLE_ARN = "arn:aws:iam::119001410604:role/GitLabCI202303301919062053#####"
    ecr_repo_url = "####.dkr.ecr.us-east-1.amazonaws.com/dev-ecr-node-app-repo"
    lb_address = "dev-tf-alb-ecs-#####.us-east-1.elb.amazonaws.com"
   ```
    * Note: The lb endpoint will be unsecure: `http://` 
4. Repeat steps for "prod" to provision a separate environment
5. At this point, OIDC, ECR, and ECS should be provisioned. If you go to the [ECS page](https://us-east-1.console.aws.amazon.com/ecs/v2/clusters?region=us-east-1), you should see a cluster and service running. It will be failing at this point because we have not pushed our images. This will come shortly.
6. Here is a quick snapshot of the OIDC resources provisioned:
  * [Identity provider with OIDC + GitLab](images/identity_provider.png)
  * [Role and policies attached](images/role_policies_attached.png)
  * [Role and Web Identity](images/role_trusted.png)

### Overview of the GitLab CI Role
There is a GitLabCI role created on each account. Examining the `gitlab.com:sub` attribute, we can see that condition is set to the specific group+project path along with the main branch. The branch value will vary of each account since we did branch+environment mapping. The development account will map to the "development" branch and production account will map to the "main branch. Also if you look at the screenshots above, you will see ECS and ECR policies attached to this role. Next, we will use the `dev` and `prod` role ARN within the GitLab CI/CD variables to make AWS calls to ECR and ECS. 
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Federated": "arn:aws:iam::160834776473:oidc-provider/gitlab.com"
            },
            "Action": "sts:AssumeRoleWithWebIdentity",
            "Condition": {
                "StringEquals": {
                    "gitlab.com:sub": "project_path:guided-explorations/aws/oidc-and-multi-account-deployment-with-ecs:ref_type:branch:ref:main"
                }
            }
        }
    ]
}
```


### Set up the GitLab Project
Now that we have the AWS environment provisioned for the `dev` and `prod` accounts, we can now push and deploy to ECS from GitLab CI/CD using OIDC.

1. The GitLab project should have been created at this point based on this [instruction](#set-up-the-gitlab-project). 
2. Please create the following [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#for-a-project) in the UI (Project -> Settings -> CI/CD -> Variables). Uncheck `Protect Variable` for now in each variable.
     * AWS_DEFAULT_REGION: `us-east-1`
     * AWS_PROFILE: `oidc` This will be referenced in our before_script to gain temporary access via OIDC.



     Development Environment: Values are from terraform apply output.
     * DEV_ECR_URL: `<ECR_OUTPUT_AFTER_TERRAFORM>` format is `####.dkr.ecr.us-east-1.amazonaws.com/repo-name`
     * DEV_ECS_ROLE_ARN: `<OIDC_OUTPUT_AFTER_TERRAFORM>` format is `arn:aws:iam::####:role/GitLabCI#####`

     Production Environment: Values are from terraform apply output.
     * PROD_ECR_URL: `<ECR_OUTPUT_AFTER_TERRAFORM>` format is `####.dkr.ecr.us-east-1.amazonaws.com/repo-name`
     * PROD_ECS_ROLE_ARN: `<OIDC_OUTPUT_AFTER_TERRAFORM>` format is `arn:aws:iam::####:role/GitLabCI#####`

### Overview of the the GitLab CI/CD file

<ins>OIDC Token</ins>

The section is included in the build and deploy job. The id_tokens attribute is customizable to your need. We set it to ECS_OIDC which will be later used for assuming the role with web identity. The `aud` value is customizable to your application. Please note this value should match the `aud` value defined in terraform.tfvars for each environment or you will receive an authorization failure. 

```yaml
id_tokens:
  ECS_OIDC:
    aud: https://ecs.customerapp.com
```

<ins>AWS Credentials and OIDC</ins>

The `.aws-prep` anchor will set the AWS credentials using the [web identity token file](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-role.html#cli-configure-role-oidc). Please make note of `ECS_OIDC` as this is a custom defined OIDC parameter using GitLab [id_tokens for OIDC](https://docs.gitlab.com/ee/ci/cloud_services/#configure-a-conditional-role-with-oidc-claims). `ECS_ROLE_ARN` is the role defined from our OIDC provisioning through terraform. This role has a policy attached to access ECR and ECS. In addition, the role is only authorized on the GitLab project we specified in the `aud` value.

```yaml
.aws-prep: &aws-prep
  - | 
    IMAGE_TAG="$(echo $CI_COMMIT_SHA | head -c 8)"
    mkdir -p ~/.aws
    echo "${ECS_OIDC}" > /tmp/web_identity_token
    echo -e "[profile oidc]\nrole_arn=${ECS_ROLE_ARN}\nweb_identity_token_file=/tmp/web_identity_token" > ~/.aws/config
```
<ins>Build and Push to ECR</ins>

The build function will build the "App" folder, then tag and push the to our ECR registry for each account depending on which branch is committed. We will use the commit SHA and latest tag here. 

<ins>Deploy to ECS</ins>

We will take the existing task definition that was provisioned and update the image to the matching commit sha of this commit. 

## Let's build and deploy to our accounts!

### Deploy to Production
1. At this point, we should have cloned the repository so "App" directory is at least in our project. The variables should be defined in the UI. No changes are required in `.gitlab-ci.yml` file. Please verify that the name of the ECS Cluster, Service, and Task definition match what was provisioned in the previous steps.
1. Go to Project -> CI/CD -> Pipelines -> Run Pipeline -> Select "main" branch. This will build and push to the production account.
1. If the pipeline was successful, we can visit the `lb_address` value for `prod` and the view should be similar to: 
<p align="center">
  <img src="images/deployment_screenshot.png" alt="isolated" width="400"/>
</p>

### Deploy to Development
1. Lets now push to our development environment. We will need to [create a "development" branch](https://docs.gitlab.com/ee/user/project/repository/branches/#create-branch) to align our build and deploy job to push to the development account.
1. After the "development" branch is created, a pipeline should automatically be triggered to build and push to the "development" account. If not, go to Project -> CI/CD -> Pipelines -> Run Pipeline -> Select "development" branch. This will build and push to the development account.
   

## Wrap up
After you have completed the tutorial, you can clean up and destroy the AWS environment. Please run the commands below to clean up each account.
```bash
cd terraform/environments/<env>
export AWS_PROFILE=<env>
terraform destroy -var-file="terraform.tfvars"
```
* If it hang's on destroying the internet_gateway, please kill the execution and try the destroy function again. TODO FIX
